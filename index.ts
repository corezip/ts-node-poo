import "reflect-metadata";
import express from "express";
import * as bodyParser from "body-parser";
import { user } from "@app/routes/user";

const app = express();
const port = 3000;
app.use(bodyParser.json());

// Configure routes
app.use("/user", user);
app.get("/", (req, res) => {
  res.send("The sedulous hyena ate the antelope!");
});

app.listen(port, () => {
  return console.log(`server is listening on localhost:${port}`);
});
