import { IUser } from "@utils/interfaces";

export class User {
  private name: string;
  private email: string;
  private password: string;
  constructor() {}

  setUser(user: IUser) {
    this.name = user.name;
    this.email = user.email;
    this.password = user.password;
  }

  getUser() {
    return { name: this.name, email: this.email, password: this.password };
  }

  getEmail() {
    return this.email;
  }

  setEmail(email: string) {
    return (this.email = email);
  }

  getName() {
    return this.name;
  }

  setName(name: string) {
    return (this.name = name);
  }

  getPassword() {
    return this.password;
  }

  setPassword(password: string) {
    return (this.password = password);
  }
}
