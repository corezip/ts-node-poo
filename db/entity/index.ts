import { User } from "./user/User";
import { Product } from "./product/Product";
import { Ticket } from "./ticket/Ticket";

const entities = [User, Ticket, Product];
export default entities;
