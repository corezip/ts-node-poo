import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Ticket } from "../ticket/Ticket";

@Entity("user", { schema: "shop" })
export class User {
  @PrimaryGeneratedColumn({
    type: "int",
    name: "id",
    comment:
      "This column represents the id of the entity. It composes the primary key of the table..",
  })
  public id: number;

  @Column("varchar", {
    length: 50,
    name: "username",
    nullable: false,
    comment: "This colum represents the username of the user.",
  })
  public userName: string;

  @Column("varchar", {
    name: "password",
    nullable: false,
    comment: "This colum represents the password of the user.",
  })
  public password: string;

  @Column("varchar", {
    name: "name",
    nullable: false,
    comment: "This column refers to the name of the user",
  })
  public name: string;

  @Column("varchar", {
    name: "email",
    nullable: false,
    comment: "This column refers to the email of the user",
  })
  public email: string;

  @CreateDateColumn({
    name: "created_on",
    comment:
      "This date field allows knowing the date of creation of the record. At the Database level it has by default the value of the date at the time of insertion and it will be in UTC format. In case the application that generates the creation of the record sends the field, the received value will be assigned.",
  })
  public creationDate: Date;

  @UpdateDateColumn({
    name: "updated_on",
    nullable: true,
    comment: "This date field allows to know the last update made on a record.",
  })
  public updateDate: Date;

  @DeleteDateColumn({
    name: "deleted_on",
    nullable: true,
    comment:
      "This date field allows to know the date of deletion of the record.  It will represent the logical deletion of the records.  It can be used as a filter to display active and inactive records.",
  })
  public deleteDate: Date | null;

  @OneToMany("Ticket", "user")
  public ticket: Ticket[];
}
