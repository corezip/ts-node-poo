import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Ticket } from "../ticket/Ticket";

@Entity("product", { schema: "shop" })
export class Product {
  @PrimaryGeneratedColumn({
    type: "int",
    name: "id",
  })
  public id: number;

  @Column("bigint", {
    name: "ticket_id",
    nullable: false,
  })
  public ticketId: string;

  @Column("varchar", {
    length: 50,
    name: "name",
    nullable: false,
  })
  public name: string;

  @Column("float", {
    name: "price",
    nullable: false,
  })
  public price: number;

  @Column("bigint", {
    name: "qty",
    nullable: false,
  })
  public qty: number;

  @CreateDateColumn({
    name: "created_on",
    comment:
      "This date field allows knowing the date of creation of the record. At the Database level it has by default the value of the date at the time of insertion and it will be in UTC format. In case the application that generates the creation of the record sends the field, the received value will be assigned.",
  })
  public creationDate: Date;

  @UpdateDateColumn({
    name: "updated_on",
    nullable: true,
    comment: "This date field allows to know the last update made on a record.",
  })
  public updateDate: Date;

  @DeleteDateColumn({
    name: "deleted_on",
    nullable: true,
    comment:
      "This date field allows to know the date of deletion of the record.  It will represent the logical deletion of the records.  It can be used as a filter to display active and inactive records.",
  })
  public deleteDate: Date | null;

  @ManyToOne("Ticket", "proveedor", {
    onDelete: "SET NULL",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "ticket_id", referencedColumnName: "id" }])
  public ticket: Ticket;
}
