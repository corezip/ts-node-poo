import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Product } from "../product/Product";
import { User } from "../user/User";

@Entity("ticket", { schema: "shop" })
export class Ticket {
  @PrimaryGeneratedColumn({
    type: "int",
    name: "id",
  })
  public id: number;

  @Column("varchar", {
    length: 50,
    name: "reference",
    nullable: false,
  })
  public userName: string;

  @Column("float", {
    name: "total",
    nullable: false,
  })
  public total: number;

  @Column("bigint", {
    name: "user_id",
    nullable: false,
  })
  public userId: string;

  @CreateDateColumn({
    name: "created_on",
    comment:
      "This date field allows knowing the date of creation of the record. At the Database level it has by default the value of the date at the time of insertion and it will be in UTC format. In case the application that generates the creation of the record sends the field, the received value will be assigned.",
  })
  public creationDate: Date;

  @UpdateDateColumn({
    name: "updated_on",
    nullable: true,
    comment: "This date field allows to know the last update made on a record.",
  })
  public updateDate: Date;

  @DeleteDateColumn({
    name: "deleted_on",
    nullable: true,
    comment:
      "This date field allows to know the date of deletion of the record.  It will represent the logical deletion of the records.  It can be used as a filter to display active and inactive records.",
  })
  public deleteDate: Date | null;

  @OneToMany("Product", "ticket")
  public product: Product[];

  @ManyToOne("User", "ticket", {
    onDelete: "SET NULL",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "id" }])
  public user: User;
}
