import "reflect-metadata";
import { DataSource } from "typeorm";

import entities from "@entity/index";
import migration from "@db/migration";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "username",
  password: "password",
  database: "rabbit",
  synchronize: true,
  logging: true,
  entities,
  migrations: migration,
  subscribers: [],
});
