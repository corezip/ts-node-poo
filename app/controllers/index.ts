import { Request, Response } from "express";
import { User } from "@utils/classes";
import { IUser } from "@utils/interfaces";

export const algo = () => console.log("ALGO");

export const createUser = (req: Request, res: Response) => {
  const userData: IUser = req.body;
  const user = new User();
  user.setUser(userData);
  const userObj = user.getUser();
  console.info("USER:", userObj);
  res.send(userObj);
};
