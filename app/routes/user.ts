import { createUser } from "@app/controllers";
import express, { Request, Response } from "express";
export const user = express.Router();

user.get("/", (request: Request, response: Response) => {
  response.send("MODO USUARIO");
});

user.post("/", createUser);
